  <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-NHWTFF5');</script>
  <!-- End Google Tag Manager -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="We are a small independent gym with a friendly active membership, located in the heart of Chiswick W4.">

  <title>{{ $page_title ?? "West4 Gym" }}</title>

  <!-- Bootstrap core CSS -->
  <!--<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">  -->
  <link href="{{asset('css/app.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="{{asset('/assets/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

  <link href="{{asset('css/cookie.css')}}" rel="stylesheet">
  <style>
      td a{
          color: #444;
          font-weight: bold;
          text-decoration: underline;
      }
  </style>
  <!-- Custom styles for this template -->
