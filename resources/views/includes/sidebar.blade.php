<div class="user-panel pt-5">
    <ul class="list-unstyled px-3">
      <li><a href="{{url('/home')}}" class="text-light text-decoration-none">Dashboard</a></li>
      <li><a href="{{url('/admin/therapists')}}" class="text-light text-decoration-none">Therapists</a></li>
      <li><a href="{{url('/admin/trainers')}}" class="text-light text-decoration-none">Trainers</a></li>
        <li><a href="{{url('/admin/page')}}" class="text-light text-decoration-none">Front Page</a></li>
        <li><a href="{{url('/admin/classes')}}" class="text-light text-decoration-none">Classes</a></li>
    </ul>
</div>
