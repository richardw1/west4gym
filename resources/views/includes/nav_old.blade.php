<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHWTFF5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
 
<!-- Navigation -->
 <div class="container-fluid banner">
  <div class="row">
    <div class=col-12>  
  <div class="float-left py-3"><a href="{{asset('/')}}"><img src="{{asset('/assets/img/west4logo.png')}}" height="60"></a></div>
  <div class="float-right pt-3"><h5><i class="fas fa-phone"></i> 0208 747 1713</h5></div>
</div>
</div>
@if (Request::is('/'))
<div class="row">
    <div class="col-12 p-1 text-center bg-warning text-dark">Gym is opening on Saturday 25th July. </div>

  </div>
@endif
</div>
 
 <nav class="navbar navbar-expand-lg nav-pills nav-fill" id="mainNav">
    <div class="container">

      <button class="navbar-toggler navbar-toggler-right mx-auto" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase mx-auto ">
        <li class="nav-item">
            <a class="nav-link {{Request::path()==='/'?'active':''}}" href="{{url('/')}}">Home</a>
          </li>
   <li class="nav-item">
            <a class="nav-link {{Request::path()==='/about'?'active':''}}" href="{{url('/')}}/about">About us</a>
          </li>
        <li class="nav-item">
            <a class="nav-link {{Request::path()==='membership'?'active':''}}" href="{{url('/')}}/membership">Membership</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{Request::path()==='classes'?'active':''}}" href="{{url('/')}}/classes">HIIT Classes</a>
          </li>

          <li class="nav-item">
          <a class="nav-link {{Request::path()==='trainers'?'active':''}}" href="{{url('/')}}/trainers">Personal Trainers</a>
          </li>
          <li class="nav-item">
          <a class="nav-link {{Request::path()==='therapists'?'active':''}}" href="{{url('/')}}/therapists">Therapists</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{Request::path()==='contact'?'active':''}}" href="{{url('/')}}/contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>


