<!-- Footer -->

    <div class="container">
    <div class="row">
    <div class="col-md-4">
    <h4>Find Us</h4>
    <p class="text-justify">
     We are in Sutton Lane North, 10 minutes walk from Gunnersbury Station,  Grove Park, Chiswick High Road and Chiswick Park.  Our postcode is W44LD. If you are still unsure we have a <a href="{{asset('/find')}}">map</a>.
    </p>
    </div>
       <div class="col-md-8 bgcolor-dark">
       <div class="row">

       <div class="col-md-6">
       <h4>Quick Links</h4>
           <ul class="list-unstyled">
           <li><a href="{{asset('/')}}">Home</a></li>
           <li><a href="{{asset('/about')}}">About us</a></li>
           <li><a href="{{asset('/therapists')}}">Therapists</a></li>
           <li><a href="{{asset('/trainers')}}">Personal Trainers</a></li>
           <li><a href="{{asset('/find')}}">Find us</a></li>
           <li><a href="{{asset('/opening')}}">Opening Times</a></li>
           </ul>
       </div>
       <div class="col-md-6">
       <h4>Follow Us</h4>
           <ul  class="list-unstyled">
           <li><i class="fab fa-facebook-f"></i> <a href="https://www.facebook.com/west4gym/"> Facebook</a></li>
           <li><i class="fab fa-twitter"></i> <a href="https://twitter.com/west4gym"> Twitter</a></li>
           <li><i class="fab fa-instagram"></i> <a href="https://www.instagram.com/west4gym">Instagram</a></li>
           </ul>
       </div>
       </div>
       </div>

    </div>


      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; West4Gym
         2019 - @php echo date("Y") @endphp
          </span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="https://twitter.com/west4gym">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="https://www.facebook.com/pages/West-4-Gym/122845017746884?fref=ts">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="https://www.instagram.com/west4gym">
                <i class="fab fa-instagram"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="{{asset('privacy')}}">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="{{asset('terms')}}">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>

