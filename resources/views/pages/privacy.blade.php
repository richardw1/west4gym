@extends('layouts.default')
@section('content')

  <!-- About -->
<!-- Header -->

  
<header>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Privacy</h2>
        </div>
     </div>
</header>
<div class="container">
     <div class="row">
        <div class="col-lg-12 pb-5">
        <p>We collect certain information or data about you when you use phplaravel-349008-1093391.cloudwaysapps.com.</p>

<p>This includes:
<ul>
<li>questions, queries or feedback you leave, including your email address if you send a message or email</li>
<li>your IP address, and details of which version of web browser you used</li>
<li>information on how you use the site, using cookies and page tagging techniques to help us improve the website</li>
</ul>
</p>
<p>This helps us to:</p>
<ul>
<li>improve the site by monitoring how you use it</li>
<li>respond to any feedback you send us, if you’ve asked us to</li>
<li>We can’t personally identify you using your data.</li>
</ul>
<h4>WHERE YOUR DATA IS STORED</h4>
<p>We store your data on our secure servers in the UK. It may also be stored outside of Europe, where it could be viewed by our staff or suppliers. By submitting your personal data, you agree to this.

<h4>KEEPING YOUR DATA SECURE</h4>
<p>Transmitting information over the internet is generally not completely secure, and we can’t guarantee the security of your data. Any data you transmit is at your own risk. We have procedures and security features in place to try and keep your data secure once we receive it. We won’t share your information with any other organisations for marketing, market research or commercial purposes, and we don’t pass on your details to other websites.

You can find out what information we hold about you, and ask us not to use any of the information we collect. Links to other websites phplaravel-349008-1093391.cloudwaysapps.com contains links to and from other websites. This privacy policy only applies to this website, and doesn’t cover other services and transactions that we link to. Following a link to another website If you go to another website from this one, read the privacy policy on that website to find out what it does with your information. Following a link to phplaravel-349008-1093391.cloudwaysapps.com from another website If you come to phplaravel-349008-1093391.cloudwaysapps.com from another website, we may receive personal information about you from the other website.</p>
</div>
      </div>
    </div>  
  @stop
