@extends('layouts.default')
@section('content')
<!-- Header -->
<header class="membership">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Contact</h2>
            <h3 class="section-subheading text-muted"></h3>
        </div>
    </div>
</header>


   
  <!-- Contact -->
 
    <div class="container">
    <div class="row">
    <div class="col-lg-12">
    <p>Please feel free to swing by the gym to get a feel of the place. We are always interested in understanding more about your fitness goals whether that be maintaining a healthy body or training for a big event. Check out our <a href="/find">opening times</a>.</p>
    </div>

    </div>


@if ($message = Session::get('success'))<div class=row>
<div class="col-lg-12">
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
</div>
</div>
@endif
      <div class="row">
        <div class="col-lg-12">
          <form action="{{ url('/contact/send')}}" method="post">
          {{csrf_field()}}
          @honeypot
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control" name="name" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" name="email" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" name="tel" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" name="content" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center py-5">
                <div id="success"></div>
                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    
   
   
    </div> <!--container-->
 
  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Contact form JavaScript -->
  <script src="{{asset('/assets/js/jqBootstrapValidation.js')}}"></script>
  @stop