@extends('layouts.default')
@section('content')

<!-- Header -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Class Timetable</h2>

        </div>
    </div>
</header>



  <!-- Contact -->

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-auto">

    <table class="table table-bordered table-primary table-responsive-sm" >

  <thead>
    <tr>
      <th scope="col">Day</th>
        <th scope="col">Description</th>
      <th scope="col">Time</th>
      <th scope="col">Charge</th>
    </tr>
  </thead>
  <tbody>
@foreach($classes as $c)
    <tr>
      <th scope="row" > {{jddayofweek($c->day-1, 1)}}</th>
 
        <td>
          @if($c->professional==null)
          {{$c->description}} 
      @else
          {{$c->description}}  with <a href="/profile/{{$c->professional->slug}}">{{$c->professional->fname}}</a>
      @endif
        </td>
      <td>{{$c->start_time}} - {{$c->end_time}}</td>
        <td>{{$c->charge? $c->charge : 'Free'}}</td>
    </tr>
@endforeach


  </tbody>
</table>


</div>
        </div>









    </div> <!--container-->

  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  @stop
