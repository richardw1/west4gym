@extends('layouts.default')
@section('content')


    <!-- Header -->
    <header class="masthead">
        <div class="container">
                <a class="btn btn-warning btn-xl text-uppercase font-weight-bold" href="{{asset('/membership')}}">Memberships available - Join today</a>
        </div>
        @push('liveStyles')
            @livewireStyles
         <!--    <script src="//unpkg.com/alpinejs" defer></script> -->

        @endpush
        
        
    </header>

<div class="container">
    <div class="row py-3">
        
       
        <x-pictureroll>
 <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/v1669137528/paul_dynamic_dgcu2h.jpg" alt="Paul lateral lunge" style="width:100%">
            

        </x-pictureroll>
        <x-pictureroll>
 <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/c_scale,h_400,w_600/v1669216488/sophie_lunge_y6y1kj.jpg" alt="Sophie doing lunge" style="width:100%">
           

        </x-pictureroll>
        <x-pictureroll>
          <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/c_crop,h_400,w_600/v1670416711/IMG_2331_tfija7.jpg"  alt="Cammy squat" style="width:100%" >
        </x-pictureroll>

        <x-pictureroll>
          <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/c_crop,h_400,w_600/v1670416657/IMG_2371_fsrao6.jpg"  alt="Sandi" style="width:100%" >
        </x-pictureroll>
        <x-pictureroll>
          <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/c_crop,h_400,w_600/v1670416622/IMG_1537_vtkg2z.jpg"  alt="Lloyd Boxing class" style="width:100%" >
        </x-pictureroll>
    
    
          <x-pictureroll>
             <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/c_scale,h_400,w_600/v1669215996/anny_class_mpafmm.jpg" alt="Anny taking a class" style="width:100%">
           
          </x-pictureroll>
      
    </div>
</div>
 


    <div class="container">
        <div class="row py-2">

            <div class="col-md-9">
                <h3>{{ $title }}</h3>
                {!! $bodyText !!}
            </div>

            <div class="col-md-3">
               <img class="img-fluid float-right" src="{{asset('/assets/img/paulandsam.jpg') }}" /> 
                <p>Paul and Sam</p>
                </div> 

        </div>
    </div>


    <x-divider />
    <div class="container ">
        <div class="row mt-2">
        <div class="col text-center">
        <h3>5 steps to create an effective exercise program.</h3>
        </div>
    </div>

    <div class="row font-xl">
            <div class="col-md bg-primary p-3 mx-2">
                          <svg xmlns="http://www.w3.org/2000/svg" class="mb-1" height="16" width="10" viewBox="0 0 256 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M160 64c0-11.8-6.5-22.6-16.9-28.2s-23-5-32.8 1.6l-96 64C-.5 111.2-4.4 131 5.4 145.8s29.7 18.7 44.4 8.9L96 123.8V416H32c-17.7 0-32 14.3-32 32s14.3 32 32 32h96 96c17.7 0 32-14.3 32-32s-14.3-32-32-32H160V64z"/></svg>
     <span> Assess Your Fitness Baseline and ensure you know your starting point.</span>
              </div>    
              <div class="col-md bg-primary p-3 mx-2">
                <svg xmlns="http://www.w3.org/2000/svg" class="mb-1" height="16" width="10" viewBox="0 0 320 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M142.9 96c-21.5 0-42.2 8.5-57.4 23.8L54.6 150.6c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L40.2 74.5C67.5 47.3 104.4 32 142.9 32C223 32 288 97 288 177.1c0 38.5-15.3 75.4-42.5 102.6L109.3 416H288c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-12.9 0-24.6-7.8-29.6-19.8s-2.2-25.7 6.9-34.9L200.2 234.5c15.2-15.2 23.8-35.9 23.8-57.4c0-44.8-36.3-81.1-81.1-81.1z"/></svg>
                
                Create Meaningful Goals. When you choose your goals, consider the reason why you want to make a change.
              </div>    
              <div class="col-md bg-primary p-3 mx-2">
                <svg xmlns="http://www.w3.org/2000/svg" class="mb-1"  height="16" width="10" viewBox="0 0 320 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M0 64C0 46.3 14.3 32 32 32H272c13.2 0 25 8.1 29.8 20.4s1.5 26.3-8.2 35.2L162.3 208H184c75.1 0 136 60.9 136 136s-60.9 136-136 136H105.4C63 480 24.2 456 5.3 418.1l-1.9-3.8c-7.9-15.8-1.5-35 14.3-42.9s35-1.5 42.9 14.3l1.9 3.8c8.1 16.3 24.8 26.5 42.9 26.5H184c39.8 0 72-32.2 72-72s-32.2-72-72-72H80c-13.2 0-25-8.1-29.8-20.4s-1.5-26.3 8.2-35.2L189.7 96H32C14.3 96 0 81.7 0 64z"/></svg>
                Make Your Action Plan.  Think of it as simply getting from A to B. Baseline to Goal.
          </div>
          <div class="col-md bg-primary p-3 mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="mb-1"  height="16" width="12" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M189 77.6c7.5-16 .7-35.1-15.3-42.6s-35.1-.7-42.6 15.3L3 322.4c-4.7 9.9-3.9 21.5 1.9 30.8S21 368 32 368H256v80c0 17.7 14.3 32 32 32s32-14.3 32-32V368h32c17.7 0 32-14.3 32-32s-14.3-32-32-32H320V160c0-17.7-14.3-32-32-32s-32 14.3-32 32V304H82.4L189 77.6z"/></svg>
           Get Started and be consistent!  Put it in your calendar like a normal appointment and don't over do it.
          </div>
          <div class="col-md bg-primary p-3 mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="mb-1"  height="16" width="10" viewBox="0 0 320 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M32.5 58.3C35.3 43.1 48.5 32 64 32H256c17.7 0 32 14.3 32 32s-14.3 32-32 32H90.7L70.3 208H184c75.1 0 136 60.9 136 136s-60.9 136-136 136H100.5c-39.4 0-75.4-22.3-93-57.5l-4.1-8.2c-7.9-15.8-1.5-35 14.3-42.9s35-1.5 42.9 14.3l4.1 8.2c6.8 13.6 20.6 22.1 35.8 22.1H184c39.8 0 72-32.2 72-72s-32.2-72-72-72H32c-9.5 0-18.5-4.2-24.6-11.5s-8.6-16.9-6.9-26.2l32-176z"/></svg>
           Track progress and modify accordingly. Setbacks are normal and sometimes life just gets in the way.
          </div>
        </div>
<div class="row mb-2">
   <div class="col"> 
    <p>Many people will feel comfortable to do this on their own but if you have any queries please speak to a member of staff.  
        Alternatively if you want some specialist support why not consider engaging one of our <a href="/trainers">Personal Trainers</a>.
    </p>
   </div>
</div>

      </div>
    <x-divider />
  
    <div class="container">
        <div class="row py-2">   
            <div class="col-md-6 order-md-last">
                <h3>Fresh Fitness Food</h3>
                <p class="my-1">How it works</p>
                <ul class="mb-0">
                    <li>Choose your fitness goal</li>
                    <li>A nutritionist will design your plan</li>
                    <li>Expert chefs prepare your food</li>
                    <li>Fresh Fitness deliver, you eat</li>
                    <li>Use the Fresh Fitness app to log your progress</li>
                </ul>
                <p class="mt-1">Go to <a class="text-underscore" href="https://try.freshfitnessfood.com/west-4-gym/"><u>freshfitnessfood</u></a> to get a £65 off a 5-day trial.</p>
            </div>       

            <div class="col-md-6 order-md-first">
                <img class="img-fluid" src="{{asset('/assets/img/freshfood.png') }}" />
            </div>
        </div> <!--end of row -->
    </div>
    
    <x-divider />

    <div class="container">
        <div class="row py-2">
            <div class="col-md-6 order-md-first">
                <h3>Personal Training</h3>
                <p>For people wishing to engage a Personal Trainer we have a number of West 4 Staff &amp; Personal Trainer 
                   associates to choose from depending on your goals. We do not have a set fee structure, these are set by the individual Trainer. 
                   You can <a href="trainers"><u>browse their profiles</u></a> and make contact with them directly in order to 
                   discuss your goals and arrange an initial session.  We would encourage you to join a Gym but you do not need to be a West4 Gym Member if you are working with one of our Personal Trainers.
                </p>
            </div>       

            <div class="col-md-6 order-md-last">
                <img class="img-fluid float-right" src="{{asset('/assets/img/sampt.png') }}" />
            </div>
        </div> <!--end of row -->
    </div> <!--end of container -->
 
    <x-divider />
    
    <div class="container">
        <div class="row pb-5">
            <div class="col-md-6 order-md-last">
                <h3>Guide Dogs for the Blind</h3>
                <p>West 4 Gym have been supporting Guide dogs for the blind for a number of years and we are currently on our third puppy in training, Fern. 
                </p>    
            </div>
            
            <div class="col-md-6 order-md-first"> 
                    <img class="img-fluid float-right" src="{{asset('/assets/img/judy-leaping-through-the-grass.jpg') }}" />
            </div>       
        </div> <!--end of row -->
     </div> <!--end of container -->
     <x-divider /> 
     <div class="container">
        <div class="row justify-content-end">
            <div class="col-md-6 order-md-last embed-responsive embed-responsive-16by9">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2484.307482595767!2d-0.2699956!3d51.48922470000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760e6df6ded4b9%3A0xb95b2f430a3217ef!2sWest%204%20Gym!5e0!3m2!1sen!2suk!4v1702035087709!5m2!1sen!2suk" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="col-md-6 order-md-first d-none d-md-block">
                <h3>Find Us</h3>
            </div>
     </div>
     </div>

     <x-divider />
     <div class="container">
        <div class="row pb-5 justify-content-center align-items-center d-flex flex-column flex-md-row">
      
   
          
            <x-iconlink>
                <img class="img-fluid" style="height:100px" src="{{asset('/assets/img/chiswickw4.png') }}" />
            </x-iconlink>  
   
            <x-iconlink class="">
                <img class="img-fluid" style="height:100px" src="{{asset('/assets/img/es.png') }}" />   
            </x-iconlink>   
   
            <x-iconlink>
                <img class="img-fluid" style="height:150px" src="{{asset('/assets/img/fave.jpg') }}" />
            </x-iconlink>      

            <x-iconlink>
                <img class="img-fluid" style="height:100px" src="{{asset('/assets/img/dt.png') }}" />
            </x-iconlink>        
        
            <x-iconlink>
             <img class="img-fluid text-center" style="height:100px" src="{{asset('/assets/img/timeout.png') }}" />
            </x-iconlink>
        </div>
     </div>
    
    
@stop
@section('myjs')
    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
@stop
@push('liveScripts')
    @livewireScripts
@endpush
