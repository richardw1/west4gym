@extends('layouts.default')
@section('content')

  <!-- About -->
<!-- Header -->

  
  <section class="page-section" id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">About us</h2>
          <h3 class="section-subheading text-muted"></h3>
        </div>
      </div>
      <div class="row">
<div class="col-lg-12">
<p>
        The West 4 Gym management team Sam Stone and Paul Lovelace took over West 4 Gym on 1st January 2016.  Sam had been the Manager for 15 years prior to taking over. Paul was originally a member and then joined as a Personal Trainer. Both are West London born and bred, with Sam originally from Kew and Paul from Chiswick. 
      </p>
</div>  

<div class="col=lg-12 mx-auto">
           <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/c_scale,h_250/v1584527921/paul_and_sam_iwmkfu.jpg">

</div>
        <div class="col-lg-12 pb-5">
       
<p>Sam and Paul have both seen the area change a lot during the last few years, but have an ongoing commitment to creating a unique space for their members - the 300 Spartans.  “The 300 Spartans captures the spirit of the club - with a personal service that enables each individual to meet their needs with a collective friendly environment”.  
        </p><p>Sam and Paul have added a Treatment Room, put in new lighting, and installed new cardio equipment.  The Treatment Room has been a success, hosting a range of Independent Therapists providing Osteopathy, rehabilitation and Sports Massage. 

        </p><p>For those occasions where two heads are greater than one we have an excellent group of Personal Trainers.
        </p><p>We run a number of High Intensity Interval Training classes - in Paul's words “If you want to get fit fast - this is the way to go”.
        </p></div>
      </div> 
    </div>
  </section>
  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  @stop
