@extends('layouts.default')
@section('content')
<!-- Header -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{ $professional->fname }} {{$professional->lname}}</h2>
            </div>
        </div>
    </div>
  <style>

   
  </style>
</header>


<div class="container">

    <div class="row py-3">
        <div class="col-md-4 pb-3">
          {!! $img_cld !!}
        </div>
        <div class="col-md-8 profile">
            {!! $professional->long_desc !!}


<div x-data="{open: false}">
 <button x-on:click="open =!open" class="rounded bg-primary">Contact {{ $professional->fname }}</button> 
 <div x-show="open" class="py-4">
            @livewire('profcontactform',['professional'=>$professional])
</div>
    </div>
</div>
      
</div>





  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  @stop
