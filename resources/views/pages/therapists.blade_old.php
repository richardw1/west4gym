@extends('layouts.default')
@section('content')
<!-- Header -->
<header class="membership">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Therapists</h2>
            <h3 class="section-subheading text-muted">We are able to offer a wide range of Therapies.</h3>
        </div>
    </div>
</header>

 
    <div class="container">
<div class="row">
<div class="col-md-6 pb-5"><h3>Therapy Room</h3><p>We have an on site Therapy Room and are able to offer a wide range of therapies, delivered through a team of Independant Tharapists.</p>


</div>
<div class="col-md-6 pb-5 bg-light">
 <h3> Booking a Session</h3>
 <p>Please contact you chosen Therapists directly to discuss pricing and availability. Contact details are provided on profile pages.  </p>
</div>
</div>
<div class="row">
            <div class="col-lg-12 text-center">

<h3 class="text-muted">Browse the Therapist profiles</h3>
</div>
</div>
<div class="row py-3">

@foreach ($partners as $partner)

     <div class=col-md-4>

        <div class="card mb-3">
            <img class="card-img-top" src="{{ $partner->image_cloud }}" alt="Card image" style="width:100%">
            <div class="card-body">
            <h4 class="card-title">{{ $partner->fname }} {{$partner->lname}}</h4>
            <p class="card-text">{{ $partner->intro }}</p>
            <a href="{{asset('/profile')}}/{{$partner->id}}" class="btn btn-primary">See Profile</a>
            </div>
        </div>

    </div>
    @if($loop->iteration % 3  == 0)
</div>
<div class="row py-3">

@endif

   @endforeach
    </div>

 
 
  


</div>    	   
	   
    </div> <!--container-->
 
  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  @stop