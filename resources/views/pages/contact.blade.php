@extends('layouts.default')
@section('content')
<!-- Header -->
<header class="membership">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Contact us</h2>
            <h3 class="section-subheading text-muted"></h3>
        </div>
    </div>
        @push('liveStyles')
            @livewireStyles
    @endpush
</header>



  <!-- Contact -->

    <div class="container">
    <div class="row">
    <div class="col-lg-12">
    <p class="contact">Please feel free to swing by the gym to get a feel of the place. We are always interested in understanding more about your fitness goals whether that be maintaining a healthy body or training for a big event. Check out our <a href="/find" >opening times</a>.</p>
    </div>

    </div>


@if ($message = Session::get('success'))<div class="row">
<div class="col-lg-12">
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
</div>
</div>
@endif
      <div class="row">
        <div class="col-lg-12">


          <form action="{{ url('/contact/send')}}" method="post" id="contact">
          @csrf
          <x-honeypot />
 
                <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    value="{{old('name')}}"
                    placeholder="Your Name *"
                    required="required"
                    data-validation-required-message="Please enter your name.">
                  <p class="help-block text-danger">{{$errors->first('name')}}</p>
                </div>
                <div class="form-group">
                    <input class="form-control"
                    name="email"
                    id="email"
                    value= "{{old('email')}}"
                    type="email"
                    placeholder="Your Email *"
                    data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger" >{{$errors->first('email')}}</p>
                </div>
                 <div class="form-group">
                    <input class="form-control"
                    name="tel"
                    id="phone"
                    value="{{old('tel')}}"
                    type="tel"
                    placeholder="Your Phone *"
                    data-validation-required-message="Please enter a phone number.">
                  <p class="help-block text-danger">{{$errors->first('tel')}}</p>
                </div>


              </div>
                    <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control"
                  name="message"
                  id="message"
                  placeholder="Your Message *
                  "data-validation-required-message="Please enter your email address." >{{old('message')}}</textarea>
                  <p class="help-block text-danger">{{$errors->first('message')}}</p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center py-5">
                <div id="success"></div>
                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>

              </div>
            </div>
          </form>

        </div>
      </div>



    </div> <!--container-->

  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>


  <!-- Contact form JavaScript -->
  <script src="{{asset('/assets/js/jqBootstrapValidation.js')}}"></script>
  @stop
@push('liveScripts')
    @livewireScripts
@endpush
