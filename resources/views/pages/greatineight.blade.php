@extends('layouts.default')
@section('content')

  <!-- About -->
<!-- Header -->
  <!-- Header -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">great in Eight</h2>
             <h3 class="section-subheading text-muted">A new Eight week Training Block to achieve sustainable results.</h3>
        </div>
    </div>
</header>

  
 
<div class="container">
  <div class="row">
  <div class="col-md-6 bg-light">

  <h3>Fitness Testing</h3>
   <p>Body Composition, aerobic condition, blood pressure and weekly weigh in.</p>
   <ul>
   <li>Access to 8 Bootcamp classes per week</li>
   <li>2 months Gym Membership</li>
   <li>Bespoke training programme</li>
   <li>One Sports Massage</li>
   <li>8 One to one personal training sessions</li>
   </ul>
  </div>
  <div class="col-md-6 bg-light">

  <h3>Health Coaching &amp; Nutrition</h3>
    <p>Support and advice from fully qualified dietician and nutritionist.</p><ul>
    <li>1 hour consultation</li>
    <li>1/2 hour follow up consultation</li>
    <li>2 Telephone consultations @ 15 minutes each</li>
    <li>Email support as needed</li>
    </ul>
   </div>
   </div>    <!-- eof row -->
 <div class="row py-5">
  <div class="col-md-12">
  <p>To achieve great results we recommend you commit to a minimum of 3 workouts per week &amp; importantly stay within the dietary guidelines.</p>
  <h5 class="text-center pt-5">Full 8 week programme available for &pound;799</h5>
  </div>
  </div>

</div>   <!-- eof container -->
  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  @stop