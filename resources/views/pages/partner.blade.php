@extends('layouts.default')
@section('content')
<!-- Header -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{ $profile->fname }} {{$profile->lname}}</h2>
            </div>
        </div>
    </div>
</header>

 
<div class="container">
    <div class="row py-3">
        <div class="col-md-4 pb-3">
            <img class="card-img-top" src="{{ $profile->image_cloud }}" alt="Card image" style="width:100%">
        </div>
        <div class="col-md-8 profile">
            {!! $profile->long_desc !!}
        </div>
                
    </div>
                                            
</div>

    	   
	   

 
  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  @stop