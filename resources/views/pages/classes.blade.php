@extends('layouts.default')
@section('content')

<!-- Header -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Class Timetable</h2>

        </div>
    </div>
</header>



  <!-- Contact -->

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-auto">

    <table class="table table-bordered" >

  <thead>
    <tr>
      <th scope="col">Day</th>
        <th scope="col">Description</th>
      <th scope="col">Time</th>
      <th scope="col">Charge</th>
    </tr>
  </thead>
  <tbody>

    <tr>
      <th scope="row" >Monday</th>
      <td>Circuit Training</td>
      <td>19:00 - 19.45</td>
      <td>Free</td>
    </tr>

    <tr>
        <th scope="row">Wednesday</th>
        <td>Boxing with <a href="/profile/25">Lloyd</a></td>
        <td>19.15 - 20.30</td>
        <td>&pound;15</td>
    </tr>
    <tr>
        <th scope="row" >Thursday</th>
        <td>Circuit Training</td>
        <td>19:00 - 19.45</td>
        <td>Free</td>
    </tr>

    <tr>
      <th scope="row">Saturday</th>
      <td>HIIT</td>
      <td>12.00 - 12.45</td>
      <td>Free</td>
    </tr>
    <tr>
        <th scope="row">Saturday</th>
        <td>Yoga with <a href="/profile/24">Anna</a></td>
        <td>7.45 - 8.45 AM</td>
        <td><a href="/profile/24">see profile</a></td>
    </tr>

  </tbody>
</table>


</div>
        </div>









    </div> <!--container-->

  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  @stop
