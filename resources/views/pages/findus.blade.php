@extends('layouts.default')
@section('content')
<!-- Header -->
@push('liveStyles')
    <style>
      html, body, #map { height: 100%; margin: 0; }
    </style>
@endpush
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Find us</h2>
          </div>
    </div>
 
</header>


   
  <!-- Contact -->
 
    <div class="container">
    <div class="row py-5">
    <div class="col-md-6">
    <div id="map"></div>
    <address class="pt-5">West4 Gym <br />Sutton Lane North<br />Chiswick W4 4LD </address>

    </div>
    <div class="col-md-6">
    <img class="float-right img-fluid max-width: 100%" src="{{asset('assets/img/edificio.jpg')}}"  alt="Responsive image" />
    </div>

    </div>




    

   
    </div> <!--container-->
 
  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

      <script>

// Initialize and add the map
function initMap() {
  // The location of Uluru
  var west4 = {lat: 51.489270, lng: -0.269980};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 12, center: west4});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: west4, map: map});
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
  <script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDThlC8o0xBWjMTbIcas9fI8l_M_oPg-WQ&callback=initMap">
   </script>
 
  @stop