@extends('layouts.default')
@section('content')
<!-- Header -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Opening Times</h2>

        </div>
    </div>
</header>



  <!-- Contact -->

    <div class="container">
 <div class="row py-5">
     <div class="col-md-6 offset-md-3">

      <table class="table table-primary table-bordered">
    <thead>
      <tr>
        <th>Day</th>
        <th>Opens </th>
        <th>Closes</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Monday</td>
        <td>7.00</td>
        <td>21.00</td>
      </tr>
      <tr>
        <td>Tuesday</td>
        <td>7.00</td>
        <td>21.00</td>
      </tr>      <tr>
        <td>Wednesday</td>
        <td>7.00</td>
        <td>21.00</td>
      </tr>      <tr>
        <td>Thursday</td>
        <td>7.00</td>
        <td>21.00</td>
      </tr>            <tr>
        <td>Friday</td>
        <td>7.00</td>
        <td>20.00</td>
      </tr>
      <tr>
        <td>Saturday</td>
        <td>9.00</td>
        <td>17.00</td>
</tr>
        <tr>
        <td>Sunday</td>
        <td>9.00</td>
        <td>17.00</td>
      </tr>
    </tbody>
  </table>
</div>
</div>

    </div> <!--container-->

  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDThlC8o0xBWjMTbIcas9fI8l_M_oPg-WQ"></script>
  @stop
