@extends('layouts.default')



@section('content')


    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Personal Trainers</h2>
                <h3 class="section-subheading text-muted pb-5">There are some occasions in life where two heads are better
                    than one.</h3>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class=col-md-3>


                <div class="col-md-12 pb-5 bg-light">
                    <h5>Booking A Personal Training Session</h5>
                    <p>Please contact your chosen Personal Trainer directly to discuss pricing and availability. Contact
                        details are provided on individual profile pages.</p>

                </div>
            </div>
            <div class="col-md-9">
                <div class="container">

                        <div class="row row-cols-1 row-cols-md-3">


                        @foreach ($partners as $partner)
                                <div class="col mb-4">
                                    <div class="card h-100">
                                        {!! $partner->image_tag !!}
                                       
                                        <div class="card-body">
                                            
                                            <h5 class="card-title">{{ $partner->fname }} {{$partner->lname}}</h5>
                                            <p class="card-text">{!!$partner->intro !!}</p>
                                        </div>
                                        <div class="m-1">
                                            <a href="{{asset('/profile')}}/{{$partner->slug}}" class="btn btn-primary w-100">
                                                {{  $partner->fname  }}'s Profile
                                            </a>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                    </div>



                
            </div>
        </div>


    </div> <!--container-->
    </div>

  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  @stop
