@extends('layouts.default')
@section('content')
<!-- Header -->
<header class="membership">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Membership</h2>
                <h3 class="section-subheading text-muted">Call or <a href="/contact" >email </a> to book a tour during opening hours.</h3>


        </div>
    </div>
</header>

    <div class="container">
        <div class="row mb-5">
            <div class="col-md-6 p-0 m-0">
                {{-- <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/v1597069910/annyhooker_pwd4y7.jpg" style="width:100%"> --}}
                <img src="https://res.cloudinary.com/west4gym-co-uk/image/upload/v1626863148/4F2BDDB0-F17D-45D8-BC31-B85E54EB5A6C.resized_nx3cqh.jpg" class="img-fluid max-width: 100%" >
            </div>
            <div class="col-md-6 p-0 m-0">

                <img  src="https://res.cloudinary.com/west4gym-co-uk/image/upload/v1626863710/07EB3D0E-9630-4FB9-AD6B-422D401AB843.resized_giguly.jpg" class="img-fluid max-width: 100%" >
            </div>
        </div>
        <div class="row text-center ">

            <div class="col-md-4">
            <span class="fa-stack fa-4x">
                <i class="fas fa-circle fa-stack-2x text-secondary"></i>
                    <strong class="fa-stack-1x fa-inverse">
                    1
                    </strong>
                </span>
                <h4 class="pt-3">Pay as you go</h4>
                <p class="text-muted">£80/month <br/>£15 Guest Day Pass</p>
            </div>
            <div class="col-md-4">
            <span class="fa-stack fa-4x">
                <i class="fas fa-circle fa-stack-2x text-secondary"></i>
                    <strong class="fa-stack-1x fa-inverse">
                    2
                    </strong>
                </span>
                <h4 class="pt-3">Individual</h4>
                <p class="text-muted">£70/month or *£750/year <br/>Payment by Direct Debit<br/> * Subject to £25 Admin Fee.</p>
            </div>
            <div class="col-md-4">
            <span class="fa-stack fa-4x">
                <i class="fas fa-circle fa-stack-2x text-secondary"></i>
                    <strong class="fa-stack-1x fa-inverse">
                    3
                    </strong>
                </span>
                <h4 class="pt-3">Junior</h4>
                <p class="text-muted">£50/month<br/>Age 16 - 18</p>
            </div>
        </div>




        <div class="row text-center">
	        <div class="col-md-12 pt-5  pb-5">
                <h3>Refreshingly flexible membership terms</h3>
                <ul class="list-unstyled">

            <li>No long term contracts. </li>
            <li>No complicated cancellation process, just drop us an email to cancel with just one payments notice.</li>
            </ul>
                <h3 class="pt-3">New Individual Member Benefits</h3></ul>
                <ul class="list-unstyled">
                <li>Two complimentary guest passes worth £20</li>

            <li>Consultation with a personal trainer</li>
            <li>Personalized training programme</li>
             <li>Applies solely to Individual Memberships</li>
        </ul>
            </div><!--col-->
	    </div><!--row-->


    </div> <!--container-->

  @stop
  @section('myjs')
    <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  @stop
