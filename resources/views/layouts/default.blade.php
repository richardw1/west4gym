<!doctype html>
<html lang="en" class="h-100">

<head>
    @include('includes.head')
    @stack('liveStyles')
 
</head>

<body id="page-top" class="d-flex flex-column h-100">
<!-- nav -->

    @include('includes.nav')


<main class="flex-shrink-0">
    @yield('content')
</main>
<footer class="footer mt-auto pt-3">
        @include('includes.footer')
    </footer>
     @yield('myjs')
@stack('liveScripts')

</body>

</html>

