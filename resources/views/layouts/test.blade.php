<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>BootCountry</title>
 
 
 
@vite(['resources/sass/app.scss', 'resources/js/app.js']) 
    <!-- Custom styles for this template -->
   
  </head>
  <body class="d-flex flex-column h-100">
  
<header>
    @include('part.nav')

</header>

<!-- Begin page content -->
<main class="flex-shrink-0">
  <div class="container">
    <div class="p-5">
    </div>
@yield('content') 
</div>
</main>

<footer class="footer mt-auto">
  
  @include('part.footer')
 
</footer>


  

  </body>
</html>