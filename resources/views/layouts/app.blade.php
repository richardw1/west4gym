<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

 
    <!-- Styles -->
    <style>
        
    #sidebar {
        min-height: 100vh;

        padding: 0;
    }
    body{
        background-color: white !important;
    }
    textarea{width: 100%;
    min-height: 150px;    }

 

</style>
 <!-- Jquery -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<!-- Summernote CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs5.min.css"/>
<!-- Summernote JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs5.min.js"></script>

 
 


@vite(['resources/sass/app.scss', 'resources/js/app.js'])
 
 

 
<style>
    [x-cloak] { display: none !important; }
</style>

    <head>
<body>
    <div id="app">

        <main>
            <div class="container-fluid">
                <div class="row">
                 
                    <div class="col-md-3 bg-dark" id="sidebar">
                        @auth
                        @include('includes.sidebar')
                         @endauth      
                    </div>
             
                    <div class="col-md-9 px-0 mb-3">
                        @include('includes.navadmin')
                        @yield('content')
                       
                    </div>
                </div>
            </div>
        </main>
    </div>

     
    <script>
$(document).ready(function() {
    $('.summernote').summernote({
        placeholder: 'Please add some text',
        tabsize: 2,
        height: 150,
        fontSize: 16,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],        
            ['insert',['link']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['codeview'],
        ]
    });
    var noteBar = $('.note-toolbar');
        noteBar.find('[data-toggle]').each(function() {
            $(this).attr('data-bs-toggle', $(this).attr('data-toggle')).removeAttr('data-toggle');
        });
});
      </script>
 
</body>
</html>
