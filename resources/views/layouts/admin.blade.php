<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.104.2">
    <title>Bootstrap v5.2</title>
 
 

@vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <!-- Custom styles for this template -->
   
  </head>
  <body class="d-flex flex-column h-100">
  
<header>
    @include('part.nav_admin')

</header>

<!-- Begin page content -->
<main class="flex-shrink-0 pb-5">
  <div class="container">
@yield('content') 
</div>
</main>




  

  </body>
</html>