<div class="container">

    <h1>Classes</h1>
        <table class="table table-responsive table-sm">
<thead>


        <tr>
            <th>Day</th>
            <th>Description</th>
            <th>Instructor</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Charge</th>
            <th></th>
        </tr>
</thead>
        <tbody>


        @foreach ($data as $d)
            <tr>
                <td>{{jddayofweek($d->day-1, 1)}}</td>
                <td>{{$d->description}}</td>

                <td>{{$d->professional->fname?? "W4" }} {{$d->professional->lname?? "Staff"}}</td>

                <td>{{ $d->start_time }}</td>
                <td>{{ $d->end_time }}</td>

                <td>{{$d->charge? $d->charge : 'Free'}}</td>
                <td>
                    <button wire:click="edit({{$d->id}})" class="btn btn-sm btn-info py-0">Edit</button> |
                    <button wire:click="destroy({{$d->id}})" class="btn btn-sm btn-danger py-0">Delete</button>


                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    <div x-cloak x-data="{ visible : @entangle('showCreate') }" >



        <div class="float-right w-1/4">

            <button class="btn btn-primary" wire:click="editor" >Add Class</button>

        </div>
        <div class="create" x-show="visible" >
            <input type="hidden" wire:model="selected_id" class="form-control">
            <div>
                <div class="form-group mt-5">
                    <label for="day">Day</label>
                    <select id="days" wire:model="day" class="form-control w-auto">
                        <option selected value="3">-- select day --</option>
                        <option value="0">Sunday</option>
                        <option value="1">Monday</option>
                        <option value="2">Tuesday</option>
                        <option value="3">Wednesday</option>
                        <option value="4">Thursday</option>
                        <option value="5">Friday</option>
                        <option value="6">Saturday</option>
                    </select>
                    @error('day') <span class="error">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" wire:model="description" class="form-control w-50"/>
                    @error('description') <span class="text-danger">{{ $message }}</span> @enderror

                </div>
            <div class="row g-3">
                <div class="form-group col-auto">
                    <label for="start_time">Start Time</label>
                    <select name="time" wire:model="start_time" class="form-control w-auto">
                        <option selected>-- select start time --</option>
                        {!! $droptime !!}

                    </select>
                    @error('start_time') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group col-auto">
                    <label for="start_time">Duration in minutes</label>
                    <input name="duration" wire:model="duration" class="form-control w-auto">
   @error('duration') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
 
            </div>      
                <div class="form-group">
                    <label for="charge">Charge Details (Text) </label>
                    <input type="text" wire:model="charge" class="form-control w-50">
                 

                </div>
                <div class="form-group mb-3">
                    <label for="user">Instructor</label>
                    <select id="instructor" wire:model="partner_id" class="form-control w-auto">
                        <option disabled> -- select --</option>
                           <option value="">Staff</option>
                            @foreach($selected as $sel)
                            <option value="{{$sel->id}}">{{$sel->fname}} {{$sel->lname}}</option>
                        @endforeach
                    </select>
                    @error('partner_id') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            @if($formbutton)
                <button wire:click="store()" class="btn btn-primary">Save</button>
                <button wire:click="close()" class="btn btn-warning">Cancel</button>
            @else
                <button wire:click="update()" class="btn btn-primary">Update</button>
                <button wire:click="close()" class="btn btn-danger">Cancel</button>
            @endif


        </div>
    </div>
</div>
