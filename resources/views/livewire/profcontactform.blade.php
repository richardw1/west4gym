<div>

    <form wire:submit.prevent="submit" action="{{ url('/professional/send')}}" method="post" >
        @csrf
        <div>

            @if (session()->has('message'))

                <div class="alert alert-success">

                    {{ session('message') }}

                </div>

            @endif

        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input class="form-control"
                           name="name"
                           id="name"
                           type="text"
                           wire:model="name"

                           placeholder="Your Name *"
                           required="required"
                           data-validation-required-message="Please enter your name.">
                    <p class="help-block text-danger">
                        @error('name') <span class="error">{{ $message }}</span> @enderror
                    </p>
                </div>
                <div class="form-group">
                    <input class="form-control"
                           name="email"

                           id="email"

                           type="email"
                           wire:model="email"
                           placeholder="Your Email *"
                           data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger" >
                        @error('email') <span class="error">{{ $message }}</span> @enderror
                     </p>
                </div>
<input  type="text" name="subject" wire:model="subject" class="d-none" >
                <div class="form-group">
                    <input class="form-control"
                           name="tel"
                           id="phone"

                           type="tel"
                           wire:model="tel"
                           placeholder="Your Phone*"
                           data-validation-required-message="Please enter a phone number.">
                    <p class="help-block text-danger">
                        @error('tel') <span class="error">{{ $message }}</span> @enderror

                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <textarea class="form-control"
                              name="message"
                              id="message"
                              placeholder="Your Message *"
                              wire:model="message"
                              data-validation-required-message="Please enter your email address." ></textarea>
                    <p class="help-block text-danger">
                        @error('message') <span class="error">{{ $message }}</span> @enderror

                    </p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 text-center py-5">
                <div id="success"></div>
                <button class="btn btn-primary btn-sm text-uppercase" type="submit">Send Message</button>

            </div>
        </div>
    </form>
   <span class="text-primary">{{$success}}</span>
</div>
