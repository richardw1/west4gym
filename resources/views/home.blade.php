@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>   You are logged in as {{ Auth::user()->name }}.</p>

                        <p>     Go ahead and manage your <a href="admin/therapists">Therapists</a> and <a href="admin/trainers">Trainers</a> sections.</p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
