@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <h5 class="mt-3">Homepage main section</h5>
                @if(count($errors))
                    <div class=row>
                        <div class="col-lg-12">
                            <div class="alert alert-success alert-dismissible fade show">
                                <button type="button" class="btn-close" data-bs-dismiss="alert">

                                </button>
                                @foreach ($errors->all() as $error)
                                    <strong>{{$error}}</strong>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                @endif

              
              

                    <hr style="width: 100%">
                    <form action="{{ url('admin/page/update')}}/{{$page->id}}" 
                          method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" type="text" name="title" value="{{ $page->title }}">
                        </div>
                        <div class="form-group">
                            <label for="bodytext">Body Content:</label>
                            <textarea class="form-control summernote" name="bodyText">{{ $page->bodyText }}</textarea>
                        </div>


                        <button type='submit' class="btn btn-primary my-3 float-right">Save</button>
                    </form>
                </div>

        </div>
        @endsection
        @section('myadminjs')
            <script>
                $(document).ready(function () {
                    $('.summernote').summernote({
                        placeholder: 'Please add some text',
                        tabsize: 2,
                        height: 250,
                        fontSizes: '16',
                        toolbar: [
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['codeview'],
                        ]
                    });
                    $("form").show();

                });

            </script>
        @stop
