@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">

                <h4>{{$snippet->description}}</h4>
            <hr />
            @if (session('success'))
                <div class=row>
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissable fade show">
                         
                            <strong>{{ session('success')}}</strong>  
                             <button type="button" class="btn-close float-end" data-bs-dismiss="alert">×</button>
                        </div>


                    </div>
                </div>
            @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <h3>
                            {{ $snippet->title }}
                        </h3>
                       {!!  $snippet->bodyText!!}
            <a href="{{url('admin/page')}}/{{$snippet->id}}/edit"><button class="btn btn-primary my-3 float-right">Edit</button></a>



        </div>
        </div>
    </div>
</div>
@endsection
