@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
        <h4>Add {{$partner_title}}</h4>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


                    <form action="{{ url('admin/professional/store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="fname">First Name</label>
                            <input type="text" name="fname" value="{{ old('fname') }}" class="form-control" placeholder="First Name" id="fname">
                          </div>
                          <div class="form-group">
                            <label for="lname">Last Name</label>
                            <input type="text" name="lname" value="{{ old('lname') }}" class="form-control" placeholder="Second Name" id="lname">
                          </div>
                          <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" id="email">
                          </div>
<input type="hidden" name="partner_type"  value="{{ $partner_type }}">
                    <h5 class="mt-3">Short Introduction (Displays on Profiles page)</h5>
                    <textarea class="summernote" name="intro" >{{ old('intro') }}</textarea>
                    <h5 class="mt-3">Description (Displays on Detail page)</h5>

                    <textarea class="summernote" name="long_desc" >{{ old('long_desc') }}</textarea>
                    <button type='submit' class="btn btn-primary my-3 float-right">Save</button>
                </form>

        </div>
    </div>
</div>
@endsection
@section('myadminjs')
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        placeholder: 'Please add some text',
        tabsize: 2,
        height: 150,
        fontSizes: ['12', '14', '16', '18', '20'],
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['codeview'],
        ]
    });
});
</script>
@stop
