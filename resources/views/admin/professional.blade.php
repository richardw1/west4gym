@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
                <h4>{{$professional->fname}} {{$professional->lname}}</h4>
                        <div class="row">
                            <div class="col-md-4">
                    {!! $cld_image!!}
                            </div>
                            <div class="col-md-4">
                                    {!!($professional->published==1)? "<span class='text-info'>Published</span>" : "<span class='text-warning'>Unpublished</span>"    !!}
                            </div>
                        </div>
                    <h5 class="mt-3">Short Introduction (Displayed on listings page)</h5>
                    <p>{!! $professional->intro !!}</p>
                    <h5 class="mt-3">Description (Displayed on individual page)</h5>
                    <p>{!!$professional->long_desc!!}</p>
                    <p>Sort Order: {{$professional->sort_order}}</p> 
                    <a href="{{url('admin/professional')}}/{{$professional->id}}/edit">   
                    <button class="btn btn-primary my-3 float-right">Edit</button></a>
        </div>
    </div>
</div>
@endsection
