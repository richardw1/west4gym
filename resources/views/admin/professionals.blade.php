@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
             
                <h4>{{$partner_title}}</h4>
             
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  
                    <table class="table">
                        <thead>
                          <tr>
                             <th></th>
                             <th>Name</th>
                            
                            <th>Status</th>
                           <th>Show</th>
                          </tr>
                        </thead> 
                        <tbody>
                          @foreach ($partners as $partner)
                          <tr>  <td>
                            {!!$partner->image_tag!!}
                          </td>
                          <td>{{$partner->fname}} {{$partner->lname}}</td>
                         
                          <td>
                            @if ($partner->published==1)
                            <span class="text-success">Published</span>
                            @else
                            <span class="text-warning">Unpublished</span>
                            @endif
                          </td>
                         <td><a href="{{url('admin/professional')}}/{{$partner->id}}">Show</a></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    
                      @if(Request::segment(2)=='therapists')        
              <a href="{{url('admin/professional/create_therapist')}}"><button  class="btn btn-primary">Add New Therapist</button></a>
              @else
               <a href="{{url('admin/professional/create_trainer')}}"><button  class="btn btn-primary">Add New Trainer</button></a>
              @endif     
            </div>

        </div>
    </div>
</div>
@endsection