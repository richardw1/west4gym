@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
                <h4>{{$professional->fname}} {{$professional->lname}}</h4>
                @if ($message = Session::get('success'))
                <div class=row>
                    <div class="col-lg-12">
                       <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                               <strong>{{ $message }}</strong>
                       </div>
                    </div>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                <div class=row>
                    <div class="col-lg-12">
                       <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        @foreach ($message as $mess)
                       <p>{{$mess}}</p>
@endforeach



                       </div>
                    </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-4">
                        {!!$cld_image!!}
                    </div>
                    <div class="col-md-4 text-left">
                    <form action="{{url('upload')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        Image Upload:
                    <input type="hidden" name="id" value="{{$professional->id}}">
                        <input type="file" name="myfile" id="fileToUpload">
                        <input type="submit" class="btn btn-primary btn-sm mt-3" name="submit" value="Upload File Now" >
                    </form>
                    </div>

                    <form action="{{ url('admin/professional/update')}}/{{$professional->id}}" method="post">
                    {{csrf_field()}}
                    <div class="col-md-4 float-right">
                        <label for="published">Published</label>
                        <input type="checkbox"
                        @if($professional->published==1)) checked 
                        @endif
                        id="published" name="published"
                        value='1'>
                    </div>
                </div>
 
                   <label for="email" class="col-sm-2 col-form-label">Email</label>
                     <input type="email" name="email" class="col-sm-2 form-control w-50" value="{{ $professional->email }}" >
                     <label for="sort_order" class="col-sm-2 col-form-label">Sort Order</label>
                    <input type='text' class="my-3 form-control w-auto" name="sort_order" value="{{ $professional->sort_order }}">                    
                    <h5 class="mt-3">Short Introduction (Displays on Profiles page)</h5>
                    <textarea class="summernote" name="intro" >{{ $professional->intro }}</textarea>
                    <h5 class="mt-3">Description (Displays on Details page)</h5>
                        <textarea class="summernote" name="long_desc" >{!! $professional->long_desc !!}</textarea>
                        <button type='submit' class="btn btn-primary btn-sm my-3 float-right">Save</button>
                    </form>
        </div>
    </div>
</div>
@endsection
@section('myadminjs')
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        placeholder: 'Please add some text',
        tabsize: 2,
        height: 150,
        fontSizes: '16',
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['codeview'],
        ]
    });

});

</script>
@stop
