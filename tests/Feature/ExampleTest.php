<?php

namespace Tests\Feature;

use App\User;

use Carbon\Carbon;
use Tests\TestCase;
use Spatie\OpeningHours\OpeningHours;
use Illuminate\Support\Facades\Config;
 

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
  

     public function testClasses()
     {
         $response = $this->get('/classes');
 
         $response->assertStatus(200);
     }   

    /**
     * @dataProvider dateProvider
     */
    public function testisopen($y, $m, $d){
        $data=Config::get('openinghours.days');
        $exceptions=Config::get('openinghours.ex');
        $openingHours = OpeningHours::create(array_merge($data,$exceptions));
         $knownDate = Carbon::create($y, $m, $d, 12);
         Carbon::setTestNow($knownDate);
            $now= Carbon::now();  // will show 2021-12-25 12:00:00
        $rr=$openingHours->isOpenAt($now);
        $this->assertTrue($rr);
    }
    public static function dateProvider(){
        return [

            [2024, 12, 27],
            [2024, 12, 28],
            [2024, 12, 29],
            [2024, 12, 30],
            [2024, 12, 31],




        ];
    }
    /**
     * @dataProvider date2Provider
     */
    public function testisclosed($y, $m, $d){
        $data=Config::get('openinghours.days');
        $exceptions=Config::get('openinghours.ex');
        $openingHours = OpeningHours::create(array_merge($data,$exceptions));
        $knownDate = Carbon::create($y, $m, $d, 12);
        Carbon::setTestNow($knownDate);
        $now= Carbon::now();  // will show 2021-12-25 12:00:00
        $rr=$openingHours->isClosedAt($now);
        $this->assertTrue($rr);
    }
    public static function date2Provider(){
        return [
            [2024, 12, 25],
            [2024, 12, 26],

            [2024, 01, 01],



        ];
    }

}
