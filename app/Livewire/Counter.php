<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

use Illuminate\Http\Request;

class Counter extends Component
{
    public $count;
    public $value;
    public function mount(Request $request)
    {
        $this->count = Storage::get('counter.txt');
        $this->value = $request->cookie('counter');
    }

    public function increment()
    {
        $this->count=Storage::get('counter.txt')+1;
        Storage::put('counter.txt',$this->count);
    }

    public function decrement()
    {
        $this->count=$this->count-1;
        Storage::put('counter.txt', $this->count);
    }

    public function render()
    {
        return view('livewire.counter');
      //   return view('livewire.counter',['cook'=>$this->value]);
    }
}
