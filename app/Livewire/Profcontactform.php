<?php

namespace App\Livewire;

use App\Mail\ProfessionalEnquiry;
use App\Professional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class Profcontactform extends Component
{

    public $name;
    public $email;
    public $tel;
    public $message;
    public $subject;
    public $success;
    public $personId;

    public function mount($professional)
    {
 $this->personId=$professional->id;
    }

    public function submit()
    {
        if(!$this->subject==""){
            return    session()->flash('message', 'Thank you for reaching out!');
        }

       $data= $this->validate([
            'name' => 'required|max:100',
            'email' => 'required|email',
            'tel' => 'required',
            'message' => 'required',

        ],
            ['tel.required' => 'A valid phone number is required']
        );

        $part = Professional::find($this->personId);

        if ($part->email) {
           $to_email = trim($part->email);
           $data['subject'] = "Enquiry direct from West4 Gym";
           }
        else
           {
           $to_email = 'sam@west4gym.co.uk';
           $data['subject'] = "Personal Training enquiry for ". $part->fname." ".$part->lname;
        }

        Mail::to($to_email)
          ->send(new ProfessionalEnquiry($data));
        session()->flash('message', 'Thank you for reaching out.');
        $this->clearfields();
    }

    private function clearFields()
        {
            $this->name = '';
            $this->email = '';
            $this->tel = '';
            $this->message = '';
        }

    public function render()
    {
        return view('livewire.profcontactform');
    }
}
