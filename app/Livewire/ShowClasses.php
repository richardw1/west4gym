<?php

namespace App\Livewire;

use App\ClassTimetable;
use App\User;
use App\Professional;

use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class ShowClasses extends Component
{
    public $showCreate=false;
    public  $formbutton=true;

    public $selected_id,
        $selected,
        $data,
        $day,
        $description,
        $start_time,
        $charge,
        $user_id=1,
        $partner_id,
        $duration=45,
        $droptime;

    protected $rules=[
    'description'=>'required',
    'start_time'=>'required|date_format:H:i',
    'duration' =>'nullable | integer',   
    //'duration' =>'nullable | date_format:i',
    'day'=> 'required|integer|between:0,6',

    ];
    protected $validationAttributes = [
        'partner_id' => 'instructor'
    ];

    protected $messages = [
         'duration' => 'Please enter minutes'
    ];

    public function mount()
    {

    }
    public function editor()
    {
        $this->showCreate = true;
        $this->formbutton=true;
        $this->resetInput();
    }
    public function close(){
        $this->showCreate=false;
    }


    private function resetInput()
    {
        $this->day='';
        $this->description="";
        $this->start_time="";
        $this->duration="45";
        $this->charge="";
        $this->partner_id='';

    }
    public function store()
    {
         $this->validate();

        ClassTimetable::create([
            'day' => $this->day,
            'description' => $this->description,
            'start_time' => $this->start_time,
            'duration' =>$this->duration,
            'charge' => ltrim($this->charge, " "),
            'partner_id' => ($this->partner_id) ? $this->partner_id : NULL,
        ]);

        $this->resetInput();
        $this->showCreate=false;
    }

    public function render()
    {
        $this->droptime=$this->get_times();
        $this->selected=Professional::orderBy('fname')
            ->where('published', '=', 1)
            ->orWhere('fname', '=', "West4")
                ->get();
        $this->data = ClassTimetable::with('professional')
           ->orderBy('day')
          ->orderBy('start_time')
          ->get();

        return view('livewire.show-classes');
    }
    
    public function get_times ($default = '19:00', $interval = '+15 minutes') {
        $output = '';
        $current = strtotime('07:00');
        $end = strtotime('20:59');
            while ($current <= $end) {
              $time = date('H:i', $current);
              $sel = ($time == $default) ? ' selected' : '';
              $output .= "<option value=\"{$time}\"{$sel}>" . date('H.i', $current) .'</option>';
              $current = strtotime($interval, $current);
            }

        return $output;
    }


    public function edit($id)
    {
        $this->formbutton=false;
        $this->showCreate=true;
        $record = ClassTimetable::findOrFail($id);
        $this->selected_id = $id;
        $this->day = $record->day;
        $this->description = $record->description;
        $this->start_time = date('H:i', strtotime($record->start_time));
        $this->duration = $record->duration;
        $this->charge = $record->charge;
        $this->partner_id = $record->partner_id;


    }
    public function destroy($id)
    {
        if ($id) {
            $record = ClassTimetable::where('id', $id);
            $record->delete();
        }
    }
    public function update(){
        $this->validate();


        if ($this->selected_id) {
            $record = ClassTimetable::find($this->selected_id);
            $record->update([
                'day' => $this->day,
                'description' => $this->description,
                //  'start_time' => $this->start_time,
                'start_time'=>date('H:i', strtotime($this->start_time)),
                'duration'=>$this->duration,
                'charge' => ltrim($this->charge," "),
                'partner_id' => ($this->partner_id) ? $this->partner_id : NULL,
            ]);

            $this->resetInput();
            $this->showCreate=false;

        }
    }
}
