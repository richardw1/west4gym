<?php

namespace App;

use App\Professional;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

class ClassTimetable extends Model
{
    protected $table='class_timetable';

    protected $guarded=[];

    public function professional(){
        return $this->belongsTo('App\Professional', 'partner_id');
    }
    public function getEndTimeAttribute()
    {
        $time = strtotime($this->start_time);

     return date("H:i", strtotime($this->duration.'minutes', $time));



    }


}
