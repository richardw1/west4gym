<?php

namespace App;

use Mews\Purifier\Casts\CleanHtml;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = array('id');

   protected $casts = [
       'bodyText' => CleanHtml::class, // cleans both when getting and setting the value
   ];
}
