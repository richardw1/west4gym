<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfessional extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'fname' => 'required',
               'lname' => 'required',
               'email' => 'required',
            'partner_type' =>'present',
               'intro' => 'required|min:100',
               'long_desc'=> 'required|min:100',
        ];


    }
    public function attributes()
    {
        return [
                 'fname' => 'First Name',
                 'lname' => 'Last Name',
                 'email' => 'Email',
                 'intro' => 'Short Description',
                 'long_desc' => 'Description'
        ];

    }
}
