<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{

    public function index()
    {
return view('pages.contact');
	   
    }
    function send(Request $request)
    {
             $data=$request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'tel'=> 'required',
            'message' =>'required'
        ],
         ['tel.required' => 'A valid phone number is required' ]
         );


/*        $data = array(
            'name'      =>  $request->name,
            'message'   =>   $request->message,
             'tel'   =>   $request->tel,
              'email'   =>   $request->email
        );*/

     Mail::to('sam@west4gym.co.uk')->send(new ContactMail($data));
     return back()->with('success', 'Thanks for contacting us!');

    }
  
}
