<?php

namespace App\Http\Controllers;

use App\Page;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Spatie\OpeningHours\OpeningHours;
use Illuminate\Support\Facades\Storage;


class FrontpageController extends Controller
{
    public function index(){

        $snip=Page::findOrFail(1);
        $title=$snip->title;
        $bodyText=$snip->bodyText;
        
      


        $data=Config::get('openinghours.days');
        $exceptions=Config::get('openinghours.ex');
        $openingHours = OpeningHours::create(array_merge($data,$exceptions));

        $now=Carbon::now();
     //  $knownDate = Carbon::create(2021, 12, 31, 12);
      //   Carbon::setTestNow($knownDate);
      //   $now= Carbon::now();  // will show 2021-12-25 12:00:00

        $openingTimes  = $openingHours->currentOpenRange($now);
        $nextOpen = $openingHours->nextOpen($now)->format('l jS F ');
        $nextOpenTime = $openingHours->nextOpen($now)->format('g:i a');

        //$open = false;
        $open=$openingHours->isOpenAt($now);

       // $value = $request->cookie('counter');
 
        return view('pages.home',
            compact('open','openingTimes', 'nextOpen','nextOpenTime','title', 'bodyText'));
       //   ->with(['open'=>$open, 'openingTimes'=>$openingTimes, 'nextOpen'=>$nextOpen, 'nextOpen2'=>$nextOpen2]);

        }


    public function setCookie(Request $request) {
     //   Storage::disk('local')->put('counter.txt', 2);
        $response = new Response(view('home'));
        $response->withCookie(cookie()->forever('counter','w4'));
        return $response;
    }

    public function getCookie(Request $request){
        $value = $request->cookie('counter');
        return view('test')->with('cook',$value);
    }

}
