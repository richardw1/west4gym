<?php

namespace App\Http\Controllers\Admin;

use App\Professional;
use Cloudinary\Uploader;
use Cloudinary\Cloudinary;
use Illuminate\Support\Str;
use Cloudinary\Tag\ImageTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cloudinary\Api\Upload\UploadApi;
use Cloudinary\Transformation\Resize;
use App\Http\Requests\StoreProfessional;
use Cloudinary\Transformation\Background;
 

class ProfessionalsController extends Controller
{

 

    public function __construct(Cloudinary $cloudinary)
    {
    $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function list_therapists()
    {
        $therapists = Professional::where('partner_type', 1)
            ->get()
            ->map(function($value)
            {
                if($value->image_tag){
                $value->image_tag= (new ImageTag($value->image_tag))
                ->resize(Resize::pad()
                ->width(50)
                ->height(50)
                );
                }
                return $value;
            })  ;  


        return view('admin.professionals')
            ->with('partners', $therapists)
            ->with('partner_title', 'Therapists');
    }


    public function list_trainers()
    {
        $therapists = Professional::where('partner_type', 2)
            ->get()
            ->map(function($value)
            {
                if($value->image_tag){
                $value->image_tag= (new ImageTag($value->image_tag))
                ->resize(Resize::pad()
                ->width(50)
                ->height(50)
                );
                }
                return $value;
            })  ;  


        return view('admin.professionals')
            ->with('partners', $therapists)
            ->with('partner_title', 'Trainers');
    }



    public function show(Professional $professional)
    {
        $imgtag = (new ImageTag($professional->image_tag))
        ->resize(Resize::pad()
        ->width(200)
        ->height(200)
        );
        //$image_url=$professional->image_tag;
        // $professional->image_cloud=cl_image_tag($image_url,   array("class"=>"img-fluid"));
        return view('admin.professional')
        ->with('cld_image', $imgtag)
            ->with('professional', $professional);

    }


    public function edit(Professional $professional)
    {
        $imgtag = (new ImageTag($professional->image_tag))
        ->resize(Resize::pad()
        ->width(200)
        ->height(200)
        );
        return view('admin.professional_form')
        ->with('cld_image', $imgtag)
            ->with('professional', $professional);
    }

//Displays the form for a new person
    public function create_therapist(Professional $professional)
    {
        return view('admin.add_professional')
            ->with('partner_title', 'Therapist')
            ->with('partner_type', '1');
    }

    public function create_trainer(Professional $professional)
    {
        return view('admin.add_professional')
            ->with('partner_title', 'Trainer')
            ->with('partner_type', '2');

    }

//Posts the result and stores in database
    public function store(StoreProfessional $request)
    {
        $slug= Str::slug($request->fname.' '.$request->lname);
        Professional::create(array_merge($request->validated(), ['slug'=>$slug]));

        $redirect_to = "admin/trainers";
        if ($request->partner_type == '1') {
            $redirect_to = "admin/therapists";
        }
        return redirect(url($redirect_to));
    }


    public function update(Professional $professional, Request $request)
    {


        $professional = Professional::find($professional->id);


        $professional->published = $request->has('published');
        $input = $request->only(['email', 'intro', 'long_desc','sort_order']);
        $professional->fill($input)->save();
//$professional->email = $request->email;
        //$professional->intro = $request->intro;
        //$professional->long_desc = $request->long_desc;
        //$professional->save();
        return redirect(url('admin/professional/' . $professional->id));
    }

   
    public function upload(Professional $professional, Request $request)
    {
        $errors = []; // Store all foreseen and unforseen errors here

        $fileExtensions = ['jpeg', 'jpg', 'png']; // Get all the file extensions

        $fileName = $_FILES['myfile']['name'];
        $fileSize = $_FILES['myfile']['size'];
        $fileTmpName = $_FILES['myfile']['tmp_name'];
        $fileType = $_FILES['myfile']['type'];

        $fileExtension = explode('.', $fileName);
        $fileExtension = end($fileExtension);
        $fileExtension = strtolower($fileExtension);

        if (isset($_POST['submit'])) {

            if (!in_array($fileExtension, $fileExtensions)) {
                $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
            }

            if ($fileSize > 1000000) {
                $errors[] = "This file is more than 1MB. Sorry, it has to be less than or equal to 1MB";
            }

            if (empty($errors)) {
                $fileTmpName = $_FILES['myfile']['tmp_name'];
                $didUpload = (new UploadApi())->upload($fileTmpName,
                    array("upload_preset" => "ajy1jltr"));

                if ($didUpload) {
                    $professional = Professional::find($request->id);
                    $professional->image_tag = $didUpload['public_id'] . "." . $didUpload['format'];;
                    $professional->save();

                    $msg = "The file " . basename($fileName) . " has been uploaded";
                    return back()->with('success', $msg);
                } else {
                    $errors[] = "An error occurred somewhere. Try again or contact the admin";
                }

            }
            return back()->withError($errors);

        }


    }

    

}
