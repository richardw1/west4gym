<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function show()
    {
        $snippet = Page::find(1);
        return view('admin.pages')
            ->with('snippet', $snippet);
    }



    public function edit(Page $page)
    {
        return view('admin.pages_form')
            ->with('page', $page);
    }
    public function update(Page $page, Request $request)
    {
        $this->validate(request(), [
            'title' => 'required|min:10',
            'bodyText' => 'required|min:100',
        ]);

        $page = Page::find($page->id);
        $input = $request->only(['title', 'bodyText']);
        $page->fill($input)->save();

        return redirect(url('admin/page/' ))
            ->with('success', 'Your page has been updated successfully!');
    }


}
