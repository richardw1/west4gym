<?php

namespace App\Http\Controllers;

use App\ClassTimetable;

use Illuminate\Http\Request;

class ClassesController extends Controller
{
 public function index(){
     $classes= ClassTimetable::with('professional')
     ->orderBy('day')
         ->orderBy('start_time')
         ->get();
      
     return view('pages.classestable')
         ->with('classes',$classes)
         ->with('page_title', 'West4 Gym | Classes');
 }
}
