<?php

namespace App\Http\Controllers;

use App\Professional;
use Illuminate\View\View;
use Cloudinary\Cloudinary;
use Cloudinary\Tag\ImageTag;
use Illuminate\Http\Request;
use App\Mail\ProfessionalEnquiry;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
 
use Cloudinary\Transformation\Resize;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Foundation\Application;

class ProfessionalsController extends Controller
{


    /**
     * @var Cloudinary
     *
     *
     */
    public $cloudinary;

    public function __construct(Cloudinary $cloudinary)
    {

    }
    public function list_therapists()
    {
        $therapists = Professional::select('*')
    
        ->where(['partner_type'=> 1, 'published'=>1])
        ->selectRAW("(CASE WHEN (sort_order >=1) THEN sort_order ELSE  100000 END) as new_order")
            ->orderBy('new_order', 'ASC')
            ->get()
            ->map(function($value)
            {
                if($value->image_tag){
                $value->image_tag= (new ImageTag($value->image_tag));
             
                }
                return $value;
            })  ;  


        return view('pages.therapists')
            ->with('partners', $therapists)
            ->with('page_title', 'West4 Gym | Therapists');
    }
    /**
     * Display a listing of the Trainers.
     */
    public function list_trainers(Request $request)
    {
        $trainers = Professional::select('*')
   
        ->where(['partner_type'=> 2, 'published'=>1])
        ->selectRAW("(CASE WHEN (sort_order >=1) THEN sort_order ELSE  100000 END) as new_order")
        ->orderBy('new_order', 'ASC')
        ->get()
        ->map(function($value)
        {
            if($value->image_tag){
            $value->image_tag= (new ImageTag($value->image_tag));
         
            }
            return $value;
        })  ;  


    return view('pages.trainers')
        ->with('partners', $trainers)
        ->with('page_title', 'West4 Gym | Trainers');
    }

    /**
     * Display a listing of the Therapists.
     *
     */
/*
    public function list_therapists()
    {
        $professional = Professional::where(['partner_type' => 1, 'published' => 1])
            ->get();
        return view('pages.therapists')
            ->with('partners', $professional)
            ->with('page_title', 'West4 Gym | Therapists');
    }
*/
    /** Send an email from a professional
     * @param Request $request
     * @return RedirectResponse
     */
    function send(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'tel' => 'required',
            'message' => 'required'
        ],
            ['tel.required' => 'A valid phone number is required']
        );

        // $part = DB::table('professionals')->where('id', $request->id)->first();
        $part = Professional::find($request->id);
        //if email is set then send to professional or send to west4
        $to_email = 'sam@west4gym.co.uk';
        $data['subject'] = "Personal Training enquiry for " . $part->fname." ".$part->lname;
      if ($part->email) {
            $to_email = trim($part->email);
            $data['subject'] = "Direct message from West4 Gym website";
        }

        Mail::to($to_email)
            ->send(new ProfessionalEnquiry($data));
        return back()->with('success', 'Thank you for contacting me.');

    }

    /**
     * Display the individual professional profile
     *
     * @param Professional $professional
     *
     * @return Application|Factory|View
     */
    public function show($slug)
    {
        
     $old_format_url=Professional::where('id', $slug)->first();
 
        if($old_format_url){
           return redirect("/profile/$old_format_url->slug", 301);
        }

       $professional=Professional::where('slug', $slug)->firstOrFail();
        $imgtag = (new ImageTag($professional->image_tag))
        ->resize(Resize::fit()
        ->width(200)
        ->height(200)
        );

        return view('pages.professional', compact('professional'))
        ->with('img_cld',$imgtag)
            ->with('page_title', config('app.name') . ' | ' . $professional->fname . ' ' . $professional->lname);
    }

}
