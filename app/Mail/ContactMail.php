<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\http\Request;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Website Enquiry')
                    ->from('no_reply@west4gym.co.uk')
                    ->bcc ('richard@west4gym.co.uk')   
                    ->replyTo($this->data['email'])
                    ->view('email.contactmail')->with('data', $this->data) ;
      
       
    }
}
