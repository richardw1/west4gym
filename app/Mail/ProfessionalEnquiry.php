<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProfessionalEnquiry extends Mailable
{
    use Queueable, SerializesModels;


    /**
    * @var mixed $data
    */

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     *@return $this
     */
    public function build()
    {
        return $this->subject($this->data['subject'])
        ->from('no_reply@west4gym.co.uk')
        ->bcc ('richard@west4gym.co.uk')
        ->replyTo($this->data['email'])
        ->view('email.professionalmail')
            ->with('data', $this->data) ;

    }
}
