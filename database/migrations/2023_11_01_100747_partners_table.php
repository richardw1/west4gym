<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname');
            $table->string('lname');
            $table->integer('sort_order')->default(1000);
            $table->string('image_cloud')->nullable();
            $table->string('image_tag');
            $table->integer('partner_type');
            $table->string('email')->nullable();
            $table->string('intro');
            $table->string('long_desc');
            $table->tinyInteger('published');
            $table->timestamps();
        });
    } 
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
