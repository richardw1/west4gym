<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_timetable', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('partner_id');
            $table->string('day');
            $table->string('description');
            $table->string('start_time');
            $table->string('duration');
            $table->string('charge');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
