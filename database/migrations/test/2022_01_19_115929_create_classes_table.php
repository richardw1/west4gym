<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_timetable', function (Blueprint $table) {
            $table->id();
            $table->string('day');
            $table->string('description');
            $table->foreignId('partner_id')->nullable();
            $table->string('start_time');
            $table->string('duration')->nullable();
            $table->string('charge');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
