<?php

use App\Professional;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/', 'FrontpageController@index');
//Route::get('/setcookie', 'FrontpageController@setCookie')->middleware('auth');




//Route::view('/', 'pages.home');

Route::view('/about', 'pages.about', ['page_title' => config('app.name').' | About us']);
Route::view('/membership', 'pages.membership', ['page_title' => config('app.name').' | Membership']);
Route::view('/privacy', 'pages.privacy');
Route::view('/terms', 'pages.terms');
Route::view('/find', 'pages.findus');
Route::view('/opening', 'pages.opening');

//Route::view('/classes', 'pages.classes', ['page_title' => config('app.name').' | Classes']);
Route::get('/classes', 'ClassesController@index');
Route::view('/gallery', 'pages.gallery', ['page_title' => config('app.name').' | Gallery']);


Route::view('/greatineight', 'pages.greatineight',['page_title' => config('app.name').' | Great in Eight']);
Route::get('/trainers','ProfessionalsController@list_trainers');
Route::get('/therapists','ProfessionalsController@list_therapists');

Route::get('/profile/{slug}/{simple?}','ProfessionalsController@show');


Route::get('/contact', 'ContactController@index');
Route::post('/contact/send', 'ContactController@send')->middleware(protspam::class);
Route::post('/professional/send', 'ProfessionalsController@send');
//Experimental


//Admin section Routes

Auth::routes(['register' => false]);

Route::middleware('auth')->group(function () {
    Route::get('/home', 'Admin\HomeController@index')->name('home');
    Route::get('/admin/therapists', 'Admin\ProfessionalsController@list_therapists');
    Route::get('/admin/trainers', 'Admin\ProfessionalsController@list_trainers');

    Route::get('/admin/professional/create_therapist', 'Admin\ProfessionalsController@create_therapist');
    Route::get('/admin/professional/create_trainer', 'Admin\ProfessionalsController@create_trainer');
    Route::get('/admin/professional/{professional}', 'Admin\ProfessionalsController@show');
    Route::get('/admin/professional/{professional}/edit', 'Admin\ProfessionalsController@edit');


    Route::post('/admin/professional/update/{professional}', 'Admin\ProfessionalsController@update');
    Route::post('/admin/professional/store', 'Admin\ProfessionalsController@store');

    Route::get('/admin/page', 'Admin\PagesController@show');
    Route::get('/admin/page/{page}/edit', 'Admin\PagesController@edit');
    Route::post('/admin/page/update/{page}', 'Admin\PagesController@update');

    Route::get('/admin/classes', 'Admin\HomeController@classes');

    Route::post('/upload', 'Admin\ProfessionalsController@upload');

    Route::get('/setcookie', 'FrontpageController@setCookie');

});
